{
    "interactionModel": {
        "languageModel": {
            "invocationName": "direct tv",
            "intents": [
                {
                    "name": "AMAZON.CancelIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.HelpIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.StopIntent",
                    "samples": []
                },
                {
                    "name": "Channel",
                    "slots": [
                        {
                            "name": "channelname",
                            "type": "channelname"
                        }
                    ],
                    "samples": [
                        "what is the channel number of {channelname}"
                    ]
                },
                {
                    "name": "Categoery",
                    "slots": [
                        {
                            "name": "channelcategoery",
                            "type": "channelcategoery"
                        }
                    ],
                    "samples": [
                        "where can i find {channelcategoery} channels",
                        "where can i find {channelcategoery}",
                        "Where do i find {channelcategoery}",
                        "Where do i find {channelcategoery} channels"
                    ]
                },
                {
                    "name": "AMAZON.NoIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.YesIntent",
                    "samples": []
                }
            ],
            "types": [
                {
                    "name": "channelname",
                    "values": [
                        {
                            "id": "NHLCenterIceHD",
                            "name": {
                                "value": "NHL Center Ice HD",
                                "synonyms": [
                                    "nhl centre ice",
                                    "NHL Center Ice HD"
                                ]
                            }
                        },
                        {
                            "id": "NFLSUNDAYTICKETtoHD",
                            "name": {
                                "value": "NFL SUNDAY TICKET to HD",
                                "synonyms": [
                                    "nfl sunday ticket",
                                    "NFL SUNDAY TICKET to HD"
                                ]
                            }
                        },
                        {
                            "id": "NBALEAGUEPASSHD",
                            "name": {
                                "value": "NBA LEAGUE PASS HD",
                                "synonyms": [
                                    "nba leauge pass",
                                    "NBA LEAGUE PASS HD"
                                ]
                            }
                        },
                        {
                            "id": "MLSDirectKickHD",
                            "name": {
                                "value": "MLS Direct Kick HD",
                                "synonyms": [
                                    "mls direct kick",
                                    "MLS Direct Kick HD"
                                ]
                            }
                        },
                        {
                            "id": "MLBEXTRAINNINGSSMHD",
                            "name": {
                                "value": "MLB EXTRA INNINGSSM HD",
                                "synonyms": [
                                    "MLB extra innings",
                                    "MLB EXTRA INNINGSSM HD"
                                ]
                            }
                        },
                        {
                            "id": "FOXSoccerPlus",
                            "name": {
                                "value": "FOX Soccer Plus",
                                "synonyms": [
                                    "fox soccer plus",
                                    "FOX Soccer Plus"
                                ]
                            }
                        },
                        {
                            "id": "DOGTV",
                            "name": {
                                "value": "DOG TV",
                                "synonyms": [
                                    "dog tv",
                                    "DOG TV"
                                ]
                            }
                        },
                        {
                            "id": "AdultProgramming",
                            "name": {
                                "value": "Adult Programming",
                                "synonyms": [
                                    "Adult Programming"
                                ]
                            }
                        },
                        {
                            "id": "YESNetworkHD",
                            "name": {
                                "value": "YES Network HD",
                                "synonyms": [
                                    "yes network",
                                    "yes network hd",
                                    "YES Network HD"
                                ]
                            }
                        },
                        {
                            "id": "TimeWarnerCableSportsNetHD",
                            "name": {
                                "value": "Time Warner Cable SportsNet HD",
                                "synonyms": [
                                    "time warner cable sportsnet",
                                    "Time Warner Cable SportsNet HD"
                                ]
                            }
                        },
                        {
                            "id": "TimeWarnerCableDeportesHD",
                            "name": {
                                "value": "Time Warner Cable Deportes HD",
                                "synonyms": [
                                    "Time Warner Cable Deportes"
                                ]
                            }
                        },
                        {
                            "id": "SportsTimeOhioHD",
                            "name": {
                                "value": "SportsTime Ohio HD",
                                "synonyms": [
                                    "sportstime ohio",
                                    "SportsTime Ohio "
                                ]
                            }
                        },
                        {
                            "id": "SNYHD",
                            "name": {
                                "value": "SNY HD",
                                "synonyms": [
                                    "yes n why hd",
                                    "yes n why",
                                    "sny"
                                ]
                            }
                        },
                        {
                            "id": "ROOTSPORTSSouthwestHD",
                            "name": {
                                "value": "ROOT SPORTS Southwest HD",
                                "synonyms": [
                                    "root sports southwest",
                                    "ROOT SPORTS Southwest HD"
                                ]
                            }
                        },
                        {
                            "id": "ROOTSPORTSRockyMountainHD",
                            "name": {
                                "value": "ROOT SPORTS Rocky Mountain HD",
                                "synonyms": [
                                    "root sports",
                                    "root sports rocky mountain",
                                    "ROOT SPORTS Rocky Mountain HD"
                                ]
                            }
                        },
                        {
                            "id": "ROOTSPORTSPittsburghHD",
                            "name": {
                                "value": "ROOT SPORTS Pittsburgh HD",
                                "synonyms": [
                                    "root sports pittsburgh",
                                    "ROOT SPORTS Pittsburgh HD"
                                ]
                            }
                        },
                        {
                            "id": "ROOTSPORTSNorthwestHD",
                            "name": {
                                "value": "ROOT SPORTS Northwest HD",
                                "synonyms": [
                                    "root sports northwest",
                                    "ROOT SPORTS Northwest HD"
                                ]
                            }
                        },
                        {
                            "id": "NESNHD",
                            "name": {
                                "value": "NESN HD",
                                "synonyms": [
                                    "nesn",
                                    "nesn hd",
                                    "NESN HD"
                                ]
                            }
                        },
                        {
                            "id": "MSGHD",
                            "name": {
                                "value": "MSG HD",
                                "synonyms": [
                                    "msg",
                                    "MSG HD"
                                ]
                            }
                        },
                        {
                            "id": "MASNHD",
                            "name": {
                                "value": "MASN HD",
                                "synonyms": [
                                    "Ma Sn",
                                    "masn ",
                                    "MASN HD"
                                ]
                            }
                        },
                        {
                            "id": "LonghornNetworkHD",
                            "name": {
                                "value": "Longhorn Network HD",
                                "synonyms": [
                                    "longhorn network hd",
                                    "Longhorn Network HD"
                                ]
                            }
                        },
                        {
                            "id": "FSNWestHD",
                            "name": {
                                "value": "FSN West HD",
                                "synonyms": [
                                    "fsn west",
                                    "FSN West HD"
                                ]
                            }
                        },
                        {
                            "id": "FSNSouthwestHD",
                            "name": {
                                "value": "FSN Southwest HD",
                                "synonyms": [
                                    "fsn southwest",
                                    "fsn southwest hd",
                                    "FSN Southwest HD"
                                ]
                            }
                        },
                        {
                            "id": "fsnsouthhd",
                            "name": {
                                "value": "FSN South HD",
                                "synonyms": [
                                    "fsn south"
                                ]
                            }
                        },
                        {
                            "id": "FSNSanDiegoHD",
                            "name": {
                                "value": "FSN San Diego HD",
                                "synonyms": [
                                    "fsn san diego"
                                ]
                            }
                        },
                        {
                            "id": "fsnohiohd",
                            "name": {
                                "value": "FSN Ohio HD",
                                "synonyms": [
                                    "fsn ohio"
                                ]
                            }
                        },
                        {
                            "id": "FSNNorthHD",
                            "name": {
                                "value": "FSN North HD",
                                "synonyms": [
                                    "fsn north",
                                    "fsn north hd",
                                    "FSN North HD"
                                ]
                            }
                        },
                        {
                            "id": "FSNMidwestHD",
                            "name": {
                                "value": "FSN Midwest HD",
                                "synonyms": [
                                    "fsn midwest hd",
                                    "FSN Midwest hd"
                                ]
                            }
                        },
                        {
                            "id": "FSNFloridaHD",
                            "name": {
                                "value": "FSN Florida HD",
                                "synonyms": [
                                    "fsn florida hd",
                                    "FSN Florida HD"
                                ]
                            }
                        },
                        {
                            "id": "FSNDetroitHD",
                            "name": {
                                "value": "FSN Detroit HD",
                                "synonyms": [
                                    "fsn detroit",
                                    "FSN Detroit HD"
                                ]
                            }
                        },
                        {
                            "id": "FSNCincinnatiHD",
                            "name": {
                                "value": "FSN Cincinnati HD",
                                "synonyms": [
                                    "fsn cincinnati",
                                    "FSN Cincinnati "
                                ]
                            }
                        },
                        {
                            "id": "FSNArizonaHD",
                            "name": {
                                "value": "FSN Arizona HD",
                                "synonyms": [
                                    "fsn arizona hd",
                                    "FSN Arizona hd"
                                ]
                            }
                        },
                        {
                            "id": "FoxSportsSunHD",
                            "name": {
                                "value": "Fox Sports Sun HD",
                                "synonyms": [
                                    "fox sports sun",
                                    "Fox Sports Sun HD"
                                ]
                            }
                        },
                        {
                            "id": "FoxSportsSoutheastHD",
                            "name": {
                                "value": "Fox Sports Southeast HD",
                                "synonyms": [
                                    "fox sports southeast",
                                    "Fox Sports Southeast "
                                ]
                            }
                        },
                        {
                            "id": "ESPNClassic",
                            "name": {
                                "value": "ESPN Classic",
                                "synonyms": [
                                    "e.s.p.n classic",
                                    "e s p n classic",
                                    "espn classic",
                                    "ESPN Classic"
                                ]
                            }
                        },
                        {
                            "id": "CBSSportsNetworkHD",
                            "name": {
                                "value": "CBS Sports Network HD",
                                "synonyms": [
                                    "cbs sports network",
                                    "CBS Sports Network HD"
                                ]
                            }
                        },
                        {
                            "id": "beINSPORTSHD",
                            "name": {
                                "value": "beIN SPORTS HD",
                                "synonyms": [
                                    "being sports hd",
                                    "bean sports hd",
                                    "bn sports hd",
                                    "bn sports",
                                    "being sports",
                                    "bean sports",
                                    "bein sports",
                                    "bein sports hd",
                                    "beIN SPORTS hd"
                                ]
                            }
                        },
                        {
                            "id": "AltitudeSportsHD",
                            "name": {
                                "value": "Altitude Sports HD",
                                "synonyms": [
                                    "altitude sports",
                                    "altitude sports hd",
                                    "Altitude Sports HD"
                                ]
                            }
                        },
                        {
                            "id": "thrillermaxhd",
                            "name": {
                                "value": "ThrillerMAX HD",
                                "synonyms": [
                                    "thriller max hd"
                                ]
                            }
                        },
                        {
                            "id": "MovieMAXEastHD",
                            "name": {
                                "value": "MovieMAX East HD",
                                "synonyms": [
                                    "moviemax east",
                                    "MovieMAX East "
                                ]
                            }
                        },
                        {
                            "id": "moremaxhd",
                            "name": {
                                "value": "MoreMAX HD",
                                "synonyms": [
                                    "moremax",
                                    "more max",
                                    "moer max",
                                    "more max hd"
                                ]
                            }
                        },
                        {
                            "id": "CINEMAXWestHD",
                            "name": {
                                "value": "CINEMAX West HD",
                                "synonyms": [
                                    "cinemax west hd",
                                    "CINEMAX West HD"
                                ]
                            }
                        },
                        {
                            "id": "cinemaxeasthd",
                            "name": {
                                "value": "CINEMAX East HD",
                                "synonyms": [
                                    "cinemax east"
                                ]
                            }
                        },
                        {
                            "id": "CINEMaXHD",
                            "name": {
                                "value": "CINEMaX HD",
                                "synonyms": [
                                    "cinemax hd",
                                    "cinemax"
                                ]
                            }
                        },
                        {
                            "id": "actionmaxeasthd",
                            "name": {
                                "value": "ActionMAX East HD",
                                "synonyms": [
                                    "action max east",
                                    "action amx east",
                                    "action max east hd"
                                ]
                            }
                        },
                        {
                            "id": "5StarMAXEastHD",
                            "name": {
                                "value": "5 StarMAX East HD",
                                "synonyms": [
                                    "five starmax east",
                                    "5 StarMAX East HD"
                                ]
                            }
                        },
                        {
                            "id": "SundanceTVHD",
                            "name": {
                                "value": "SundanceTV HD",
                                "synonyms": [
                                    "sundance tv",
                                    "sun dance tv",
                                    "SundanceTV "
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIMEWOMENHD",
                            "name": {
                                "value": "SHOWTIME WOMEN  HD",
                                "synonyms": [
                                    "showtime women",
                                    "SHOWTIME WOMEN  "
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIMESHOWCASEHD",
                            "name": {
                                "value": "SHOWTIME SHOWCASE HD",
                                "synonyms": [
                                    "showtime showcase"
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIMENEXTHD",
                            "name": {
                                "value": "SHOWTIME NEXT  HD",
                                "synonyms": [
                                    "showtime next",
                                    "SHOWTIME NEXT "
                                ]
                            }
                        },
                        {
                            "id": "showtimeextremehd",
                            "name": {
                                "value": "SHOWTIME EXTREME HD",
                                "synonyms": [
                                    "showtime extreme "
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIMEBEYONDHD",
                            "name": {
                                "value": "SHOWTIME BEYOND  HD",
                                "synonyms": [
                                    "showtime beyond",
                                    "SHOWTIME BEYOND  "
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIME2HD",
                            "name": {
                                "value": "SHOWTIME 2 HD",
                                "synonyms": [
                                    "showtime two"
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIMEWESTHD",
                            "name": {
                                "value": "SHOWTIME WEST HD",
                                "synonyms": [
                                    "showtime west",
                                    "SHOWTIME WEST "
                                ]
                            }
                        },
                        {
                            "id": "SHOWTIMEEASTHD",
                            "name": {
                                "value": "SHOWTIME EAST HD",
                                "synonyms": [
                                    "showtime east",
                                    "SHOWTIME EAST "
                                ]
                            }
                        },
                        {
                            "id": "STARZKidsandFamilyHD",
                            "name": {
                                "value": "STARZ Kids  and  Family HD",
                                "synonyms": [
                                    "starz kids and family",
                                    "stars kids and family",
                                    "STARZ Kids  and  Family HD"
                                ]
                            }
                        },
                        {
                            "id": "STARZInBlackHD",
                            "name": {
                                "value": "STARZ In Black HD",
                                "synonyms": [
                                    "stars in black"
                                ]
                            }
                        },
                        {
                            "id": "STARZEdgeHD",
                            "name": {
                                "value": "STARZ Edge HD",
                                "synonyms": [
                                    "stars edge",
                                    "STARZ Edge "
                                ]
                            }
                        },
                        {
                            "id": "STARZComedyHD",
                            "name": {
                                "value": "STARZ Comedy HD",
                                "synonyms": [
                                    "stars comedy",
                                    "stars comedy hd",
                                    "STARZ Comedy HD"
                                ]
                            }
                        },
                        {
                            "id": "starzcinemahd",
                            "name": {
                                "value": "STARZ Cinema HD",
                                "synonyms": [
                                    "stars cinema",
                                    "starz cinema"
                                ]
                            }
                        },
                        {
                            "id": "STARZWestHD",
                            "name": {
                                "value": "STARZ West HD",
                                "synonyms": [
                                    "stars west",
                                    "stars west hd",
                                    "STARZ West HD"
                                ]
                            }
                        },
                        {
                            "id": "starzeasthd",
                            "name": {
                                "value": "STARZ East HD",
                                "synonyms": [
                                    "stars east hd",
                                    "stars east",
                                    "starz east"
                                ]
                            }
                        },
                        {
                            "id": "enocorewesterns",
                            "name": {
                                "value": "ENCORE Westerns",
                                "synonyms": [
                                    "encore westerns",
                                    "encore westerns"
                                ]
                            }
                        },
                        {
                            "id": "ENCORESuspense",
                            "name": {
                                "value": "ENCORE Suspense",
                                "synonyms": [
                                    "encore suspense",
                                    "ENCORE Suspense"
                                ]
                            }
                        },
                        {
                            "id": "encorefamily",
                            "name": {
                                "value": "ENCORE Family",
                                "synonyms": [
                                    "encore family",
                                    "enocre fmaily"
                                ]
                            }
                        },
                        {
                            "id": "ENCOREClassic",
                            "name": {
                                "value": "ENCORE Classic",
                                "synonyms": [
                                    "encore classic",
                                    "ENCORE Classic"
                                ]
                            }
                        },
                        {
                            "id": "ENCOREBlack",
                            "name": {
                                "value": "ENCORE Black",
                                "synonyms": [
                                    "encore black"
                                ]
                            }
                        },
                        {
                            "id": "ENCOREActionHD",
                            "name": {
                                "value": "ENCORE Action HD",
                                "synonyms": [
                                    "encore action",
                                    "Encore action"
                                ]
                            }
                        },
                        {
                            "id": "ENCOREWestHD",
                            "name": {
                                "value": "ENCORE West HD",
                                "synonyms": [
                                    "encore west",
                                    "enocre west",
                                    "ENCORE West "
                                ]
                            }
                        },
                        {
                            "id": "ENCOREEastHD",
                            "name": {
                                "value": "ENCORE East HD",
                                "synonyms": [
                                    "encore east"
                                ]
                            }
                        },
                        {
                            "id": "HBOZoneHD",
                            "name": {
                                "value": "HBO Zone HD",
                                "synonyms": [
                                    "hbo zone hd"
                                ]
                            }
                        },
                        {
                            "id": "HBOSignatureHD",
                            "name": {
                                "value": "HBO Signature HD",
                                "synonyms": [
                                    "hbo signature",
                                    "HBO Signature "
                                ]
                            }
                        },
                        {
                            "id": "HBOLatinoHD",
                            "name": {
                                "value": "HBO Latino HD",
                                "synonyms": [
                                    "hbo latino",
                                    "HBO Latino "
                                ]
                            }
                        },
                        {
                            "id": "HBOFAMILYWESTHD",
                            "name": {
                                "value": "HBO FAMILY WEST HD",
                                "synonyms": [
                                    "HBO family west",
                                    "hbo family west"
                                ]
                            }
                        },
                        {
                            "id": "HBOFamilyEastHD",
                            "name": {
                                "value": "HBO Family East HD",
                                "synonyms": [
                                    "hbo family east",
                                    "HBO family east "
                                ]
                            }
                        },
                        {
                            "id": "HBOComedyHD",
                            "name": {
                                "value": "HBO Comedy HD",
                                "synonyms": [
                                    "hbo comedy",
                                    "HBO comedy",
                                    "HBO Comedy "
                                ]
                            }
                        },
                        {
                            "id": "HBO2WestHD",
                            "name": {
                                "value": "HBO2 West HD",
                                "synonyms": [
                                    "hbo 2 west hd",
                                    "hbo two west",
                                    "HBO two West HD"
                                ]
                            }
                        },
                        {
                            "id": "HBO2EastHD",
                            "name": {
                                "value": "HBO2 East HD",
                                "synonyms": [
                                    "hbo two east hd",
                                    "hbo two east",
                                    "HBO2 East "
                                ]
                            }
                        },
                        {
                            "id": "HBOWestHD",
                            "name": {
                                "value": "HBO West HD",
                                "synonyms": [
                                    "hbo",
                                    "hbo west",
                                    "H B O west",
                                    "HBO west"
                                ]
                            }
                        },
                        {
                            "id": "HBOEastHD",
                            "name": {
                                "value": "HBO East HD",
                                "synonyms": [
                                    "hbo",
                                    "h.b.o east",
                                    "h b o east",
                                    "hbo east",
                                    "HBO East "
                                ]
                            }
                        },
                        {
                            "id": "UniversalHD",
                            "name": {
                                "value": "Universal HD",
                                "synonyms": [
                                    "universal",
                                    "Universal"
                                ]
                            }
                        },
                        {
                            "id": "SmithsonianChannelHD",
                            "name": {
                                "value": "Smithsonian Channel HD",
                                "synonyms": [
                                    "smithsonian",
                                    "smithsonian channel "
                                ]
                            }
                        },
                        {
                            "id": "MGMHD",
                            "name": {
                                "value": "MGM HD",
                                "synonyms": [
                                    "M G M",
                                    "m g m",
                                    "mgm"
                                ]
                            }
                        },
                        {
                            "id": "HDNetMoviesHD",
                            "name": {
                                "value": "HDNet Movies HD",
                                "synonyms": [
                                    "hd net movies",
                                    "net movies"
                                ]
                            }
                        },
                        {
                            "id": "WorldHarvestTelevisionWHT",
                            "name": {
                                "value": "World Harvest Television WHT",
                                "synonyms": [
                                    "world harvest television",
                                    "world harvest",
                                    "harvest",
                                    "harvest television",
                                    "world harvest",
                                    "WHT",
                                    "World Harvest Television WHT"
                                ]
                            }
                        },
                        {
                            "id": "theworldnetwork",
                            "name": {
                                "value": "The World Network",
                                "synonyms": [
                                    "the world network",
                                    "world network"
                                ]
                            }
                        },
                        {
                            "id": "wgnamericahd",
                            "name": {
                                "value": "WGN america HD",
                                "synonyms": [
                                    "wgn america",
                                    "WGN america"
                                ]
                            }
                        },
                        {
                            "id": "weathernationhd",
                            "name": {
                                "value": "Weathernation HD",
                                "synonyms": [
                                    "weather nation hd",
                                    "weather nation"
                                ]
                            }
                        },
                        {
                            "id": "theweatherchannelhd",
                            "name": {
                                "value": "The weather channel hd",
                                "synonyms": [
                                    "weather",
                                    "the weather",
                                    "weather channel",
                                    "the weather channel"
                                ]
                            }
                        },
                        {
                            "id": "Wetvhd",
                            "name": {
                                "value": "We tv hd",
                                "synonyms": [
                                    "v tv",
                                    "we tv",
                                    "we tv hd"
                                ]
                            }
                        },
                        {
                            "id": "VH1classic",
                            "name": {
                                "value": "VH1 classic",
                                "synonyms": [
                                    "vh1 classic",
                                    "vh one classic",
                                    "vh one",
                                    "vh one classic",
                                    "VH1 classic"
                                ]
                            }
                        },
                        {
                            "id": "VH1D",
                            "name": {
                                "value": "VH1 HD",
                                "synonyms": [
                                    "vh1 hd",
                                    "vh1 one",
                                    "vh1",
                                    "vh1 hd",
                                    "vh one",
                                    "VH one",
                                    "V H one "
                                ]
                            }
                        },
                        {
                            "id": "velocityhd",
                            "name": {
                                "value": "Velocity HD",
                                "synonyms": [
                                    "velocity hd"
                                ]
                            }
                        },
                        {
                            "id": "USAnetworkhd",
                            "name": {
                                "value": "USA network HD",
                                "synonyms": [
                                    "USA network"
                                ]
                            }
                        },
                        {
                            "id": "uplift",
                            "name": {
                                "value": "Uplift"
                            }
                        },
                        {
                            "id": "up",
                            "name": {
                                "value": "UP",
                                "synonyms": [
                                    "U P",
                                    "up"
                                ]
                            }
                        },
                        {
                            "id": "UnivisionEastHD",
                            "name": {
                                "value": "Univision East HD",
                                "synonyms": [
                                    "univision east",
                                    "univision east hd"
                                ]
                            }
                        },
                        {
                            "id": "UnivisionDeportesNetworkHD",
                            "name": {
                                "value": "Univision Deportes Network HD",
                                "synonyms": [
                                    "univision deportes network",
                                    "Univision Deportes Network",
                                    "univision deportes network hd"
                                ]
                            }
                        },
                        {
                            "id": "UniMasHD",
                            "name": {
                                "value": "UniMas HD",
                                "synonyms": [
                                    "unimass",
                                    "unimas",
                                    "unimas hd",
                                    "unimass hd",
                                    "UniMass HD"
                                ]
                            }
                        },
                        {
                            "id": "tvg",
                            "name": {
                                "value": "TVG",
                                "synonyms": [
                                    "tea v gee",
                                    "tvg",
                                    "TVG",
                                    "t v g",
                                    "Tea v gee"
                                ]
                            }
                        },
                        {
                            "id": "tvone",
                            "name": {
                                "value": "Tv One",
                                "synonyms": [
                                    "tv one",
                                    "tv 1"
                                ]
                            }
                        },
                        {
                            "id": "tvlandhd",
                            "name": {
                                "value": "TV Land HD",
                                "synonyms": [
                                    "tv land hd"
                                ]
                            }
                        },
                        {
                            "id": "trutvhd",
                            "name": {
                                "value": "truTV HD",
                                "synonyms": [
                                    "tru tv hd",
                                    "true tv hd"
                                ]
                            }
                        },
                        {
                            "id": "NBCUniverseHD",
                            "name": {
                                "value": "NBC Universe HD",
                                "synonyms": [
                                    "nbc universe",
                                    "n bea sea univers",
                                    "n bea see unniverse",
                                    "nbc universe hd"
                                ]
                            }
                        },
                        {
                            "id": "NBCSportsNetworkHD",
                            "name": {
                                "value": "NBC Sports Network HD",
                                "synonyms": [
                                    "n bee sea sports network",
                                    "n bea sea sports network",
                                    "nbc sports network",
                                    "NBC Sports Network HD"
                                ]
                            }
                        },
                        {
                            "id": "NBATVHD",
                            "name": {
                                "value": "NBA TV HD",
                                "synonyms": [
                                    "nba tv hd",
                                    "NBA tv",
                                    "nba tv"
                                ]
                            }
                        },
                        {
                            "id": "NationalGeographicChannelHD",
                            "name": {
                                "value": "National Geographic Channel HD",
                                "synonyms": [
                                    "nat geo hd",
                                    "nat geo",
                                    "National Geographic Channel "
                                ]
                            }
                        },
                        {
                            "id": "natgeowildhd",
                            "name": {
                                "value": "Nat Geo WILD HD",
                                "synonyms": [
                                    "nat geo wild",
                                    "nat geo wild"
                                ]
                            }
                        },
                        {
                            "id": "nasatv",
                            "name": {
                                "value": "NASA TV",
                                "synonyms": [
                                    "nasa tv",
                                    "nasa"
                                ]
                            }
                        },
                        {
                            "id": "MTV2HD",
                            "name": {
                                "value": "MTV2 HD",
                                "synonyms": [
                                    "mtv two hd",
                                    "m tv two hd",
                                    "MTV two",
                                    "mtvtwo",
                                    "mtv two",
                                    "mtv2",
                                    "mtv 2",
                                    "mtv two"
                                ]
                            }
                        },
                        {
                            "id": "MTVHD",
                            "name": {
                                "value": "MTV HD",
                                "synonyms": [
                                    "MTV",
                                    "mtv",
                                    "m tv",
                                    "M TV"
                                ]
                            }
                        },
                        {
                            "id": "MSNBCHD",
                            "name": {
                                "value": "MSNBC HD",
                                "synonyms": [
                                    "msn bc",
                                    "msn bee sea",
                                    "M yes N bea see",
                                    "m yes n bee sea",
                                    "msnbc",
                                    "MSNBC",
                                    "M S N B C"
                                ]
                            }
                        },
                        {
                            "id": "MLBNetworkHD",
                            "name": {
                                "value": "MLB Network HD",
                                "synonyms": [
                                    "MLB network",
                                    "mlb network ",
                                    "MLB Network HD"
                                ]
                            }
                        },
                        {
                            "id": "MAVTV",
                            "name": {
                                "value": "MAVTV",
                                "synonyms": [
                                    "m av tv",
                                    "m a v tv",
                                    "mav tv",
                                    "mav"
                                ]
                            }
                        },
                        {
                            "id": "Logo",
                            "name": {
                                "value": "Logo"
                            }
                        },
                        {
                            "id": "LMNHD",
                            "name": {
                                "value": "LMN HD",
                                "synonyms": [
                                    "l m n hd",
                                    "L M N HD",
                                    "L M N"
                                ]
                            }
                        },
                        {
                            "id": "LiquidationChannel",
                            "name": {
                                "value": "Liquidation Channel"
                            }
                        },
                        {
                            "id": "LinkTV",
                            "name": {
                                "value": "Link TV"
                            }
                        },
                        {
                            "id": "LifetimeHD",
                            "name": {
                                "value": "Lifetime HD"
                            }
                        },
                        {
                            "id": "JusticeCentralHD",
                            "name": {
                                "value": "Justice Central HD",
                                "synonyms": [
                                    "justice central",
                                    "justice central HD"
                                ]
                            }
                        },
                        {
                            "id": "JewishLifeTelevision",
                            "name": {
                                "value": "Jewish Life Television"
                            }
                        },
                        {
                            "id": "JewelryTelevision",
                            "name": {
                                "value": "Jewelry Television"
                            }
                        },
                        {
                            "id": "IONTelevisionWest",
                            "name": {
                                "value": "ION Television West",
                                "synonyms": [
                                    "ion television west",
                                    "eye on television west",
                                    "ION television west"
                                ]
                            }
                        },
                        {
                            "id": "IONTelevisionEastHD",
                            "name": {
                                "value": "ION Television East HD",
                                "synonyms": [
                                    "eye on television east hd",
                                    "eye on television east",
                                    "ion television east",
                                    "ION Television East "
                                ]
                            }
                        },
                        {
                            "id": "InvestigationDiscoveryHD",
                            "name": {
                                "value": "Investigation Discovery HD",
                                "synonyms": [
                                    "investigation discovery",
                                    "Investigation Discovery "
                                ]
                            }
                        },
                        {
                            "id": "INSP",
                            "name": {
                                "value": "INSP",
                                "synonyms": [
                                    "I N yes pee",
                                    "I N S P",
                                    "I N S O"
                                ]
                            }
                        },
                        {
                            "id": "IFCHD",
                            "name": {
                                "value": "IFC HD",
                                "synonyms": [
                                    "I F C HD"
                                ]
                            }
                        },
                        {
                            "id": "HSN",
                            "name": {
                                "value": "HSN",
                                "synonyms": [
                                    "hsn",
                                    "HSN",
                                    "H S N",
                                    "h s n",
                                    "H yes n"
                                ]
                            }
                        },
                        {
                            "id": "HopeChannel",
                            "name": {
                                "value": "Hope Channel",
                                "synonyms": [
                                    "hope channel",
                                    "hope",
                                    "Hope "
                                ]
                            }
                        },
                        {
                            "id": "HLNHD",
                            "name": {
                                "value": "HLN HD",
                                "synonyms": [
                                    "H L N HD",
                                    "h l n hd",
                                    "hln",
                                    "HLN",
                                    "h l n",
                                    "H L N"
                                ]
                            }
                        },
                        {
                            "id": "HITNTV",
                            "name": {
                                "value": "HITN TV",
                                "synonyms": [
                                    "hitn",
                                    "h i t n",
                                    "h eye tea n",
                                    "H eye tea n",
                                    "HITN "
                                ]
                            }
                        },
                        {
                            "id": "HISTORYHD",
                            "name": {
                                "value": "HISTORY HD",
                                "synonyms": [
                                    "history",
                                    "HISTORY "
                                ]
                            }
                        },
                        {
                            "id": "HallmarkMoviesandMysteriesHD",
                            "name": {
                                "value": "Hallmark Movies  and  Mysteries HD",
                                "synonyms": [
                                    "hallmark movies and mysteries",
                                    "hallmark movies  and  mysteries "
                                ]
                            }
                        },
                        {
                            "id": "HGTVHD",
                            "name": {
                                "value": "HGTV HD",
                                "synonyms": [
                                    "hedge g tv",
                                    "hitch tv",
                                    "hg tea v",
                                    "h g tea v",
                                    "h gee tea v",
                                    "h g t v",
                                    "hgtv",
                                    "HGTV",
                                    "H G T V"
                                ]
                            }
                        },
                        {
                            "id": "hallmarkchannelhd",
                            "name": {
                                "value": "Hallmark Channel HD",
                                "synonyms": [
                                    "hallmark channel",
                                    "hallmark channel hd"
                                ]
                            }
                        },
                        {
                            "id": "H2HD",
                            "name": {
                                "value": "H2 HD",
                                "synonyms": [
                                    "h two hd",
                                    "h two"
                                ]
                            }
                        },
                        {
                            "id": "GSN",
                            "name": {
                                "value": "GSN",
                                "synonyms": [
                                    "ji yes n",
                                    "gee yes n",
                                    "Gee yes n",
                                    "gsn",
                                    "g s n"
                                ]
                            }
                        },
                        {
                            "id": "GreatAmericanCountryGAC",
                            "name": {
                                "value": "Great American Country GAC",
                                "synonyms": [
                                    "g a c",
                                    "G A C",
                                    "great american country",
                                    "great american country GAC"
                                ]
                            }
                        },
                        {
                            "id": "GolfChannelHD",
                            "name": {
                                "value": "Golf Channel HD",
                                "synonyms": [
                                    "golf channel",
                                    "golf channel hd"
                                ]
                            }
                        },
                        {
                            "id": "GODTV",
                            "name": {
                                "value": "GOD tv",
                                "synonyms": [
                                    "G O D tv",
                                    "GOD TV",
                                    "God Tv",
                                    "god tv"
                                ]
                            }
                        },
                        {
                            "id": "GemShoppingNetwork",
                            "name": {
                                "value": "Gem Shopping Network",
                                "synonyms": [
                                    "gem shopping network"
                                ]
                            }
                        },
                        {
                            "id": "GEBamerica",
                            "name": {
                                "value": "GEB America",
                                "synonyms": [
                                    "g e b america",
                                    "geb america",
                                    "jab america",
                                    "jeb ameriva",
                                    "G E B america"
                                ]
                            }
                        },
                        {
                            "id": "GalavisionHD",
                            "name": {
                                "value": "Galavision HD",
                                "synonyms": [
                                    "galavision",
                                    "galavision hd"
                                ]
                            }
                        },
                        {
                            "id": "fyihd",
                            "name": {
                                "value": "fyi hd",
                                "synonyms": [
                                    "f why eye",
                                    "f y i"
                                ]
                            }
                        },
                        {
                            "id": "FXXHD",
                            "name": {
                                "value": "FXX HD",
                                "synonyms": [
                                    "f x x",
                                    "fxx",
                                    "F double x"
                                ]
                            }
                        },
                        {
                            "id": "FXMovieChannelHD",
                            "name": {
                                "value": "FX Movie Channel HD",
                                "synonyms": [
                                    "fx movie channel",
                                    "FX Movie Channel ",
                                    "fx movie channel hd"
                                ]
                            }
                        },
                        {
                            "id": "FXHD",
                            "name": {
                                "value": "FX HD",
                                "synonyms": [
                                    "fX",
                                    "Fx",
                                    "FX",
                                    "fx"
                                ]
                            }
                        },
                        {
                            "id": "Fusion5hd",
                            "name": {
                                "value": "Fusion5 HD",
                                "synonyms": [
                                    "fusion 5 hd",
                                    "fusion five hd",
                                    "fusion 5",
                                    "fusion five "
                                ]
                            }
                        },
                        {
                            "id": "fusehd",
                            "name": {
                                "value": "Fuse HD",
                                "synonyms": [
                                    "fuse"
                                ]
                            }
                        },
                        {
                            "id": "Freeformhd",
                            "name": {
                                "value": "Freeform HD",
                                "synonyms": [
                                    "freeform "
                                ]
                            }
                        },
                        {
                            "id": "FreeSpeechTV5HD",
                            "name": {
                                "value": "Free Speech TV5 HD",
                                "synonyms": [
                                    "free speech tv5",
                                    "free speech tv five",
                                    "Free Speech tv5"
                                ]
                            }
                        },
                        {
                            "id": "FOXSports2HD",
                            "name": {
                                "value": "FOX Sports 2 HD",
                                "synonyms": [
                                    "fox sports 2",
                                    "fox sports two",
                                    "fox sports 2 hd"
                                ]
                            }
                        },
                        {
                            "id": "FOXSports1HD",
                            "name": {
                                "value": "FOX Sports 1 HD",
                                "synonyms": [
                                    "fox sports 1",
                                    "fox sports one",
                                    "FOX Sports 1 "
                                ]
                            }
                        },
                        {
                            "id": "FOXNewsChannelHD",
                            "name": {
                                "value": "FOX News Channel HD",
                                "synonyms": [
                                    "fox news channel",
                                    "fox neews channel",
                                    "FOX News Channel "
                                ]
                            }
                        },
                        {
                            "id": "FOXBusinessNetworkHD",
                            "name": {
                                "value": "FOX Business Network HD",
                                "synonyms": [
                                    "fox business network ",
                                    "FOX Business Network "
                                ]
                            }
                        },
                        {
                            "id": "Foodnetworkhd",
                            "name": {
                                "value": "Food Network HD",
                                "synonyms": [
                                    "food network",
                                    "Food network"
                                ]
                            }
                        },
                        {
                            "id": "EWTN",
                            "name": {
                                "value": "EWTN",
                                "synonyms": [
                                    "e w tea n",
                                    "e w tea n",
                                    "e w t n"
                                ]
                            }
                        },
                        {
                            "id": "EVINE",
                            "name": {
                                "value": "EVINE",
                                "synonyms": [
                                    "e vine",
                                    "e wine",
                                    "ewine"
                                ]
                            }
                        },
                        {
                            "id": "Esquirenetwork",
                            "name": {
                                "value": "Esquire Network",
                                "synonyms": [
                                    "yes square network"
                                ]
                            }
                        },
                        {
                            "id": "ESPNUHD",
                            "name": {
                                "value": "ESPNU HD",
                                "synonyms": [
                                    "e.s.p.n.u hd",
                                    "e.s.p.n.u",
                                    "ESPNU",
                                    "espnu",
                                    "e yes pee n u ",
                                    "ESPNU HD",
                                    "espnu hd"
                                ]
                            }
                        },
                        {
                            "id": "ESPNnewsHD",
                            "name": {
                                "value": "ESPNnews HD",
                                "synonyms": [
                                    "e.s.p.n news",
                                    "esp news hd",
                                    "e yes p news",
                                    "ESP news"
                                ]
                            }
                        },
                        {
                            "id": "ESPN2HD",
                            "name": {
                                "value": "ESPN2 HD",
                                "synonyms": [
                                    "e.s.p.n 2 hd",
                                    "e.s.p.n 2",
                                    "ESPN 2",
                                    "espn two hd",
                                    "e yes pee n 2 hd",
                                    "e yes pee n 2",
                                    "espn 2",
                                    "espn two",
                                    "E S P N two "
                                ]
                            }
                        },
                        {
                            "id": "ESPNHD",
                            "name": {
                                "value": "ESPN HD",
                                "synonyms": [
                                    "e.s.p.n.",
                                    "e.s.p.n hd",
                                    "e.s.p.n",
                                    "E S P N HD",
                                    "e yes pee n hd",
                                    "e s p n hd"
                                ]
                            }
                        },
                        {
                            "id": "Enlace",
                            "name": {
                                "value": "Enlace",
                                "synonyms": [
                                    "enlace"
                                ]
                            }
                        },
                        {
                            "id": "ElReyHD",
                            "name": {
                                "value": "El Rey HD",
                                "synonyms": [
                                    "l rei hd",
                                    "l rey hd",
                                    "l rey hd",
                                    "el rey hd"
                                ]
                            }
                        },
                        {
                            "id": "E!HD",
                            "name": {
                                "value": "E!HD",
                                "synonyms": [
                                    "entertainment hd",
                                    "entertainment "
                                ]
                            }
                        },
                        {
                            "id": "DIYNetworkHD",
                            "name": {
                                "value": "DIY Network HD",
                                "synonyms": [
                                    "d eye y network",
                                    "dee eye network",
                                    "d eye why network",
                                    "d eye y network",
                                    "diy network",
                                    "diy network hd",
                                    "DIY Network HD"
                                ]
                            }
                        },
                        {
                            "id": "DisneyXDHD",
                            "name": {
                                "value": "Disney XD HD",
                                "synonyms": [
                                    "disney xd",
                                    "disney ex d",
                                    "Disney xd"
                                ]
                            }
                        },
                        {
                            "id": "DisneyJuniorHD",
                            "name": {
                                "value": "Disney Junior HD",
                                "synonyms": [
                                    "disney junior hd",
                                    "disney junior",
                                    "Disney Junior "
                                ]
                            }
                        },
                        {
                            "id": "DisneyChannelWest",
                            "name": {
                                "value": "Disney Channel West",
                                "synonyms": [
                                    "disney channel west"
                                ]
                            }
                        },
                        {
                            "id": "DisneyChannelEastHD",
                            "name": {
                                "value": "Disney Channel East HD",
                                "synonyms": [
                                    "disney channel east",
                                    "Disney Channel East "
                                ]
                            }
                        },
                        {
                            "id": "DiscoveryLife",
                            "name": {
                                "value": "Discovery Life",
                                "synonyms": [
                                    "discovery life"
                                ]
                            }
                        },
                        {
                            "id": "DiscoveryFamilyChannel",
                            "name": {
                                "value": "Discovery Family Channel",
                                "synonyms": [
                                    "discovery family ",
                                    "discovery family channel",
                                    "Discovery Family "
                                ]
                            }
                        },
                        {
                            "id": "discoveryhd",
                            "name": {
                                "value": "discovery hd",
                                "synonyms": [
                                    "discovery",
                                    "Discovery HD"
                                ]
                            }
                        },
                        {
                            "id": "destinationamericahd",
                            "name": {
                                "value": "Destination America HD",
                                "synonyms": [
                                    "destination america"
                                ]
                            }
                        },
                        {
                            "id": "daystar",
                            "name": {
                                "value": "Daystar",
                                "synonyms": [
                                    "day star"
                                ]
                            }
                        },
                        {
                            "id": "ctn",
                            "name": {
                                "value": "CTN",
                                "synonyms": [
                                    "c t n",
                                    "see tea n",
                                    "see tee n"
                                ]
                            }
                        },
                        {
                            "id": "cspan2",
                            "name": {
                                "value": "c span 2",
                                "synonyms": [
                                    "See Span 2",
                                    "see span 2",
                                    "see span two"
                                ]
                            }
                        },
                        {
                            "id": "Cspan",
                            "name": {
                                "value": "C span",
                                "synonyms": [
                                    "see span",
                                    "C span",
                                    "C SPAN"
                                ]
                            }
                        },
                        {
                            "id": "cookingchannelhd",
                            "name": {
                                "value": "Cooking Channel HD",
                                "synonyms": [
                                    "Cooking Channel",
                                    "cooking channel",
                                    "cooking channel"
                                ]
                            }
                        },
                        {
                            "id": "ComedyTV5HD",
                            "name": {
                                "value": "Comedy TV5 HD",
                                "synonyms": [
                                    "comedy tv5",
                                    "comedy TV5 hd"
                                ]
                            }
                        },
                        {
                            "id": "COmedycentralHD",
                            "name": {
                                "value": "Comedy Central HD",
                                "synonyms": [
                                    "comedy central",
                                    "Comedy central"
                                ]
                            }
                        },
                        {
                            "id": "CNNHD",
                            "name": {
                                "value": "CNN HD",
                                "synonyms": [
                                    "CNN",
                                    "cnn"
                                ]
                            }
                        },
                        {
                            "id": "cnbcworld",
                            "name": {
                                "value": "cnbc world",
                                "synonyms": [
                                    "CNBC world",
                                    "cnbc world"
                                ]
                            }
                        },
                        {
                            "id": "CNBCHD",
                            "name": {
                                "value": "CNBC HD",
                                "synonyms": [
                                    "cnbc",
                                    "see nbc",
                                    "see n b c",
                                    "c n b c",
                                    "cnbc",
                                    " C N B C",
                                    "CNBC"
                                ]
                            }
                        },
                        {
                            "id": "CMTHD",
                            "name": {
                                "value": "CMT HD",
                                "synonyms": [
                                    "see m t hd",
                                    "see mt hd",
                                    "see mt",
                                    "CMT HD",
                                    "Cmt hd",
                                    "cmt hd"
                                ]
                            }
                        },
                        {
                            "id": "cloo",
                            "name": {
                                "value": "cloo",
                                "synonyms": [
                                    "clooo",
                                    "clow",
                                    "Clue",
                                    "Cloo"
                                ]
                            }
                        },
                        {
                            "id": "ChurchChannel",
                            "name": {
                                "value": "Church Channel",
                                "synonyms": [
                                    "church channel"
                                ]
                            }
                        },
                        {
                            "id": "Chiller",
                            "name": {
                                "value": "chiller",
                                "synonyms": [
                                    "Chiller",
                                    "chiller",
                                    "chiller"
                                ]
                            }
                        },
                        {
                            "id": "Centric",
                            "name": {
                                "value": "Centric",
                                "synonyms": [
                                    "centric"
                                ]
                            }
                        },
                        {
                            "id": "CartoonNetworkWest",
                            "name": {
                                "value": "Cartoon Network West",
                                "synonyms": [
                                    "cartoon network",
                                    "cartoon network west",
                                    "CartoonNetworkWest"
                                ]
                            }
                        },
                        {
                            "id": "CelebrityShoppingNetwork",
                            "name": {
                                "value": "Celebrity Shopping Network",
                                "synonyms": [
                                    "celebrity shopping network"
                                ]
                            }
                        },
                        {
                            "id": "CartoonNetworkEastHD",
                            "name": {
                                "value": "Cartoon Network East HD",
                                "synonyms": [
                                    "cartoon network east",
                                    "Cartoon Network East ",
                                    "cartoon network east HD"
                                ]
                            }
                        },
                        {
                            "id": "CANALONCE6",
                            "name": {
                                "value": "CANAL ONCE6",
                                "synonyms": [
                                    "canal once",
                                    "canal once6"
                                ]
                            }
                        },
                        {
                            "id": "BYUtv",
                            "name": {
                                "value": "BYUtv",
                                "synonyms": [
                                    "beeyu tv",
                                    "bee y you tv",
                                    "B Y U tv"
                                ]
                            }
                        },
                        {
                            "id": "BTNHD",
                            "name": {
                                "value": "BTN HD",
                                "synonyms": [
                                    "BTN",
                                    "btn",
                                    "btn hd"
                                ]
                            }
                        },
                        {
                            "id": "BravoHD",
                            "name": {
                                "value": "Bravo HD",
                                "synonyms": [
                                    "bravo",
                                    "bravo hd"
                                ]
                            }
                        },
                        {
                            "id": "Boomerang",
                            "name": {
                                "value": "Boomerang",
                                "synonyms": [
                                    "boomerang"
                                ]
                            }
                        },
                        {
                            "id": "BloombergTVHD",
                            "name": {
                                "value": "Bloomberg TV HD",
                                "synonyms": [
                                    "bloomberg tv ",
                                    "Bloomberg TV ",
                                    "bloomberg tv hd"
                                ]
                            }
                        },
                        {
                            "id": "BETHD",
                            "name": {
                                "value": "Bet HD",
                                "synonyms": [
                                    "bet hd",
                                    "Bet"
                                ]
                            }
                        },
                        {
                            "id": "BBCWorldNews5HD",
                            "name": {
                                "value": "BBC World News5 HD",
                                "synonyms": [
                                    "world news5",
                                    "bbc world news5",
                                    "BBC World News5 ",
                                    "bbc world news5 hd",
                                    "BBC world news5 hD"
                                ]
                            }
                        },
                        {
                            "id": "BBCAmericaHD",
                            "name": {
                                "value": "BBC America HD",
                                "synonyms": [
                                    "bbc america",
                                    "BBC america"
                                ]
                            }
                        },
                        {
                            "id": "BabyFirstTV5",
                            "name": {
                                "value": "BabyFirst TV5",
                                "synonyms": [
                                    "tv5",
                                    "baby first",
                                    "baby first tv5"
                                ]
                            }
                        },
                        {
                            "id": "AXSTV5HD",
                            "name": {
                                "value": "AXS TV5 HD",
                                "synonyms": [
                                    "a x s tv5",
                                    "axs tv5",
                                    "AXS TV5 "
                                ]
                            }
                        },
                        {
                            "id": "AUDIENCEHD",
                            "name": {
                                "value": "AUDIENCE HD",
                                "synonyms": [
                                    "audience",
                                    "AUDIENCE "
                                ]
                            }
                        },
                        {
                            "id": "AmericasAuctionNetwork",
                            "name": {
                                "value": "Americas Auction Network",
                                "synonyms": [
                                    "americas auction network"
                                ]
                            }
                        },
                        {
                            "id": "aspirehd",
                            "name": {
                                "value": "Aspire Hd",
                                "synonyms": [
                                    "Aspire HD",
                                    "aspire",
                                    "Aspire"
                                ]
                            }
                        },
                        {
                            "id": "AnimalPlanetHd",
                            "name": {
                                "value": "Animal Planet HD",
                                "synonyms": [
                                    "animal planet",
                                    "Animal planet"
                                ]
                            }
                        },
                        {
                            "id": "AmericanHeroesChannelHD",
                            "name": {
                                "value": "American Heroes Channel HD",
                                "synonyms": [
                                    "american heroes channel ",
                                    "American Heroes Channel"
                                ]
                            }
                        },
                        {
                            "id": "AMCHD",
                            "name": {
                                "value": "AMC HD",
                                "synonyms": [
                                    "a m c",
                                    "amc",
                                    "AMC"
                                ]
                            }
                        },
                        {
                            "id": "AlJazeeraAmericaHD",
                            "name": {
                                "value": "Al Jazeera America HD",
                                "synonyms": [
                                    "al jazeera america",
                                    "Al Jazeera America "
                                ]
                            }
                        },
                        {
                            "id": "AandEHD",
                            "name": {
                                "value": "A and E HD",
                                "synonyms": [
                                    "a and e",
                                    "A and E"
                                ]
                            }
                        },
                        {
                            "id": "DIRECTVCINEMAInformation",
                            "name": {
                                "value": "DIRECTV CINEMA Information",
                                "synonyms": [
                                    "direct tv cinema information",
                                    "Direct Tv cinema information"
                                ]
                            }
                        }
                    ]
                },
                {
                    "name": "channelcategoery",
                    "values": [
                        {
                            "id": "InternationalProgramming",
                            "name": {
                                "value": "International Programming",
                                "synonyms": [
                                    "programming",
                                    "international"
                                ]
                            }
                        },
                        {
                            "id": "DIRECTVOnDemand",
                            "name": {
                                "value": "DIRECTV On Demand",
                                "synonyms": [
                                    "on demand",
                                    "direct tv"
                                ]
                            }
                        },
                        {
                            "id": "SonicTapMusicChannels",
                            "name": {
                                "value": "SonicTap Music Channels",
                                "synonyms": [
                                    "music",
                                    "music channels",
                                    "sonic tap",
                                    "SonicTapMusicChannels"
                                ]
                            }
                        },
                        {
                            "id": "SportsSubscriptionsandInteractiveExperiences",
                            "name": {
                                "value": "Sports Subscriptions and Interactive Experiences",
                                "synonyms": [
                                    "interactive experiences",
                                    "interactive",
                                    "sports subscriptions"
                                ]
                            }
                        },
                        {
                            "id": "RegionalSportsNetworks",
                            "name": {
                                "value": "Regional Sports Networks",
                                "synonyms": [
                                    "regional sports",
                                    "Regional Sports"
                                ]
                            }
                        },
                        {
                            "id": "PremiumChannels",
                            "name": {
                                "value": "Premium Channels",
                                "synonyms": [
                                    "premium",
                                    "Premium"
                                ]
                            }
                        },
                        {
                            "id": "SpanishlanguageProgramming",
                            "name": {
                                "value": "Spanish language Programming",
                                "synonyms": [
                                    "spanish language"
                                ]
                            }
                        },
                        {
                            "id": "CultureandNews",
                            "name": {
                                "value": "Culture and News",
                                "synonyms": [
                                    "news",
                                    "Culture "
                                ]
                            }
                        },
                        {
                            "id": "EducationandEntertainment",
                            "name": {
                                "value": "Education and Entertainment",
                                "synonyms": [
                                    "entertainment",
                                    "education"
                                ]
                            }
                        },
                        {
                            "id": "CustomerInformation",
                            "name": {
                                "value": "Customer Information"
                            }
                        },
                        {
                            "id": "LocalChannels",
                            "name": {
                                "value": "Local Channels"
                            }
                        },
                        {
                            "id": "DIRECTVCINEMAMoviesandPPVEvents",
                            "name": {
                                "value": "DIRECTV CINEMA Movies and PPV Events",
                                "synonyms": [
                                    "Direct cinema movies",
                                    "PPV events"
                                ]
                            }
                        }
                    ]
                }
            ]
        }
    }
}
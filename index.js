var channel = require('./DTH.js') // importing the JS file into channel//
var finder = require('./finder.js') // importing the JS file into channel//
const Alexa = require('alexa-sdk');


exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.registerHandlers(handlers);
    alexa.appId = "amzn1.ask.skill.4b4b286d-49f1-4bfc-acd6-04d047131aa6"; // APP_ID is your skill id which can be found in the Amazon developer console where you create the skill.
    alexa.execute();
};

const handlers = {
    "LaunchRequest": function () {
        console.log("Inside launch request");
        var r = 'launch'
        this.attributes.r = r;


        this.response.speak("Hello, welcome to your direct tv guide. Having trouble remembering the channel numbers ?. Dont worry, I have got you covered. Tell me what are you looking for ? ")
            .listen('Say, What is the channel number of E S P N ')
            .shouldEndSession(false);
        this.emit(":responseReady");
        
    },
    //get user name and tv guide  if present.
    // "Database" : function() {
    //     if(register == undefined){
    // register name and tv guide.
    // get the name and store it in database. IF it is not there.
    // } 
    // else {
    // Hello name and which channel number do you want to know on our  home dish connection

    //}
    // },


    'Channel': function () {
        console.log("inside channel intent");
        var speechText;



        for (var key in channel) {
            console.log("inside for loop" + key);

            for (var b in channel[key]) {
                console.log(b);
                for (var value in channel[key][b]) {
                    if (this.event.request.intent.slots.channelname.resolutions.resolutionsPerAuthority[0].values != undefined) {
                        console.log("inside if testing");
                        var c = this.event.request.intent.slots.channelname.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                        console.log(c);
                        var t = c.replace(/\s/g, '');
                        var d = value.replace(/\s/g, '');
                        console.log(`${d} and ${t}`)
                        if (t.toLowerCase() == d.toLowerCase()) {
                            console.log("value obtained in if");
                            speechText = `${channel[key][b][value]}`;
                            console.log(speechText);
                            break;
                        }
                    }

                }
            }

        }

        if (speechText == undefined) {
            var r = 'channel1';
            this.attributes.r = r;

            this.response.speak("Sorry, I did not get you, can you please repeat the channel name ")
                .listen('Which channel number do you want to know ?')
                .shouldEndSession(false);
            this.emit(':responseReady');
            var r = 'channel1';
            this.attributes.r = r;

        } 
        else {
            var r = 'channel2';
            this.attributes.r = r;
            this.attributes.channel = speechText;

            this.response.speak(`Its on channel number ${speechText} <break time="2s"/> Do you need anything else ? `)
                .listen('Do you want to know about any other channel number ?')
                .shouldEndSession(false);
            this.emit(':responseReady');
           

        }
    },

    'Categoery': function () {
        var speechText;
        for (var key in finder) {
            console.log("inside for loop of" + key);
            if (this.event.request.intent.slots.channelcategoery.resolutions.resolutionsPerAuthority[0].values != undefined) {
                for (var b in finder[key]) {
                    var c = this.event.request.intent.slots.channelcategoery.resolutions.resolutionsPerAuthority[0].values[0].value.id;
                    console.log(c);
                    var t = c.replace(/\s/g, '');
                    console.log(t);
                    var k = b.replace(/\s/g, '');
                    console.log(k + "and " + t);
                    console.log(key);
                    if (k.toLowerCase() == t.toLowerCase()) {
                        speechText = finder[key][b];
                        var sort = c;
                    }
                }
            }
        } 

        if (speechText == undefined || sort == undefined) {
            var r = 'category' ;
            this.attributes.r = r;
            this.response.speak("Sorry, I did not get you, can you please repeat the channel category")
                .listen('Which channel number do you want to know ?')
                .shouldEndSession(false);
            this.emit(':responseReady');
            
        }
        else {
            var r = 'category2';
            this.attributes.r = r;
            this.attributes.sort1 = sort;
            this.attributes.speechText1 = speechText ;
            this.response.speak(`You may find the ${sort} channels,  from channel number ${speechText}. <break time="2s"/> Anything else you need to know ? `)
                .listen('Say, no to exit')
                .shouldEndSession(false);
            this.emit(':responseReady');
           

        }

    },







    "AMAZON.NoIntent": function () {
        this.response.speak('Thanks for using your Direct TV Guide. I am here for you, whenever you need. ')
            .shouldEndSession(true);
        this.emit(':responseReady');

    },

    "AMAZON.YesIntent": function () {
        var r = 'yes';
        this.attributes.r = r;
        this.response.speak(`Which channel number or channel category you want to know about ?. <break time="2s"/> If you need any help, say help.`)
            .shouldEndSession(false);
        this.emit(':responseReady');


    },



    "AMAZON.CancelIntent": function () {
        this.response.speak('Thanks for using your Direct TV Guide. I am here for you, whenever you need. ')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    "AMAZON.HelpIntent": function () {
        var r = 'help' ;
        this.attributes.r = r;
        this.response.speak(`No worries, i will help you out. To know about a channel number,  ask , what is the channel number of bloomberg tv or any channel of your choice. <break time="1s"/>  Furthermore, you can also find out about a particular category of channels. Ask,  where can i find regional sports networks or music channels ?. `)
            .listen("Say, what is the channel number of MTV HD")
            .shouldEndSession(false);
        this.emit(':responseReady')
    },

    "AMAZON.StopIntent": function () {
        this.response.speak('Thanks for using your direct tv guide. I am here for you, whenever you need.')
            .shouldEndSession(true);
        this.emit(':responseReady');
    },

    "Repeat" : function() {
        if(this.attributes.r == 'launch'){
            this.response.speak("Hello, welcome to your direct tv guide. Having trouble remembering the channel numbers ?. Dont worry, I have got you covered. Tell me what are you looking for ? ")
            .listen('Say, What is the channel number of E S P N ')
            .shouldEndSession(false);
        this.emit(":responseReady");

        }
        if(this.attributes.r == 'channel1'){
            this.response.speak("Sorry, I did not get you, can you please repeat the channel name ")
            .listen('Which channel number do you want to know ?')
            .shouldEndSession(false);
        this.emit(':responseReady');


        }if(this.attributes.r == 'channel2'){
            this.response.speak(`Its on channel number ${this.attributes.channel} <break time="2s"/> Do you need anything else ? `)
            .listen('Do you want to know about any other channel number ?')
            .shouldEndSession(false);
        this.emit(':responseReady');


        }if(this.attributes.r == 'category'){
            this.response.speak("Sorry, I did not get you, can you please repeat the channel category")
            .listen('Which channel number do you want to know ?')
            .shouldEndSession(false);
        this.emit(':responseReady');

        }if(this.attributes.r == 'category2'){
            this.response.speak(` You may find the ${this.attributes.sort1} channels,  from channel number ${this.attributes.speechText1}. <break time="2s"/> Anything else you need to know ? `)
            .listen('Say, no to exit')
            .shouldEndSession(false);
        this.emit(':responseReady');

        }if(this.attributes.r == 'yes'){
            this.response.speak('Which channel number or channel categeory you want to know about ?')
            .shouldEndSession(false);
        this.emit(':responseReady');

        }if(this.attributes.r == 'help'){
            this.response.speak(`No worries, i will help you out. To know about a channel number,  ask , what is the channel number of bloomberg tv or any channel of your choice. <break time="1s"/>  Furthermore, you can also find out about a particular category of channels. Ask,  where can i find regional sports networks or music channels ?. `)
            .listen("Say, what is the channel number of MTV HD")
            .shouldEndSession(false);
        this.emit(':responseReady')


        }


    },

    'Unhandled': function () {
        this.response.speak('Sorry! I do not know that. Try, what is the channel number of CNBC')
            .listen('Try, what is the channel number of CNBC')
            .shouldEndSession(false);
        this.emit(':responseReady');
    }


}